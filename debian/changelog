xtermcontrol (3.8-5) unstable; urgency=medium

  * Make the build reproducible. (Closes: #994976)

 -- Chris Lamb <lamby@debian.org>  Sat, 30 Apr 2022 17:30:12 -0700

xtermcontrol (3.8-4) unstable; urgency=low

  * QA upload.
  * Add xfonts-base to autopkgtest dependencies to avoid xterm warning on
    STDERR which makes autopkgtest fail (but not dh_auto_test during the
    build).
  * Add missing "QA upload" to previous debian/changelog entry.

 -- Axel Beckert <abe@debian.org>  Sun, 03 Oct 2021 22:11:53 +0200

xtermcontrol (3.8-3) unstable; urgency=low

  * QA upload.
  * Also add xauth to autopkgtest dependencies.

 -- Axel Beckert <abe@debian.org>  Wed, 22 Sep 2021 21:08:55 +0200

xtermcontrol (3.8-2) unstable; urgency=low

  * QA upload.
  * Add Vcs-* headers for new packaging repo on Salsa.

 -- Axel Beckert <abe@debian.org>  Wed, 22 Sep 2021 02:40:50 +0200

xtermcontrol (3.8-1) unstable; urgency=low

  * QA upload.
  * Set Maintainer to Debian QA Group. (See #994646)
  * Bump debian/watch version from unsupported 2 to current 4.
  * Run "wrap-and-sort -a".
  * Use "dh" sequencer (rewrite debian/rules) and debhelper-compat=13.
    + Run dh_auto_test inside an xterm under a virtual X server. Add
      build-dependencies on xterm, xauth and xvfb for that.
    + Capture STDOUT and exit code of dh_auto_test running inside an
      xterm. Output them later and make a separate command to exit with
      that exit code since xterm doesn't relay exit codes. (See #994848)
    + Enable all hardening flags.
  * Remove trailing whitespace from ancienct debian/changelog entries.
  * Add a Homepage header and switch homepage URL in d/copyright to HTTPS.
  * Set "Rules-Requires-Root: no".
  * Switch source format from old 1.0 to "3.0 (quilt)".
  * Update debian/watch to avoid trailing dot in version number.
  * Import new upstream release 3.8.
    + Update copyright years in debian/copyright.
  * Convert debian/copyright to machine-readable DEP5 format.
    + Also mention LGPL of getopt.*.
  * Declare compliance with Debian Policy 4.6.0.
  * Enable autopkgtest.

 -- Axel Beckert <abe@debian.org>  Wed, 22 Sep 2021 02:21:51 +0200

xtermcontrol (3.6-1) unstable; urgency=medium

  * New upstream release

 -- Robert Lemmen <robertle@semistable.com>  Sat, 09 Mar 2019 11:05:00 +0100

xtermcontrol (3.3-2) unstable; urgency=medium

  * Maintenance release
    * Bumped debhelper version
    * Bumped standards-version

 -- Robert Lemmen <robertle@semistable.com>  Tue, 06 Mar 2018 21:46:49 +0100

xtermcontrol (3.3-1) unstable; urgency=medium

  * New upstream release
  * Bumped standards-version

 -- Robert Lemmen <robertle@semistable.com>  Thu, 23 Jun 2016 20:33:07 +0100

xtermcontrol (3.1-1) unstable; urgency=low

  * New upstream release
  * Upgrade to latest standsrds-version (no changes)
  * Fix some minus-vs-hyphen cases in manpage
  * Switch on hardning flags in build

 -- Robert Lemmen <robertle@semistable.com>  Sat, 22 Jun 2013 17:55:54 +0100

xtermcontrol (3.0-1) unstable; urgency=low

  * New upstream release
  * Fixed some (but by far not all) lintian warnings:
    * add misc:Depends to control file
    * add debian/source/format
    * build-arch and build-indep in debian/rules
    * remove boilerplate from debian/watch

 -- Robert Lemmen <robertle@semistable.com>  Fri, 07 Jun 2013 12:46:44 +0100

xtermcontrol (2.10-1) unstable; urgency=low

  * New upstream release (closes: #544396)
  * Bumped Standards-Version and di some related cleanups

 -- Robert Lemmen <robertle@semistable.com>  Thu, 22 Oct 2009 10:15:15 +0100

xtermcontrol (2.9-2) unstable; urgency=low

  * Fixed the manual page for whatis (closes: #462974)
  * Bumped standards version

 -- Robert Lemmen <robertle@semistable.com>  Fri,  7 Mar 2008 11:12:58 +0000

xtermcontrol (2.9-1) unstable; urgency=low

  * New upstream release (closes: #391270, #437108)

 -- Robert Lemmen <robertle@semistable.com>  Tue,  4 Sep 2007 15:31:00 +0200

xtermcontrol (2.8-1) unstable; urgency=low

  * New upstream release (closes: #437108)

 -- Robert Lemmen <robertle@semistable.com>  Tue, 28 Aug 2007 10:38:26 +0100

xtermcontrol (2.7-1) unstable; urgency=low

  * New upstream release

 -- Robert Lemmen <robertle@semistable.com>  Sun, 26 Dec 2004 18:01:22 +0100

xtermcontrol (2.6-1) unstable; urgency=low

  * New upstream release

 -- Robert Lemmen <robertle@semistable.com>  Sun, 17 Oct 2004 18:21:30 +0200

xtermcontrol (2.4-1) unstable; urgency=low

  * New upstream release
  * Fixed description (closes: #238724)

 -- Robert Lemmen <robertle@semistable.com>  Mon, 26 Apr 2004 17:05:30 +0200

xtermcontrol (2.3-3) unstable; urgency=low

  * updated debhelper dependency to reflact what I can actually test

 -- Robert Lemmen <robertle@semistable.com>  Wed,  3 Mar 2004 13:25:04 +0100

xtermcontrol (2.3-2) unstable; urgency=low

  * cleaned up debian/rules
  * added dependency on xterm
  * fixed debian/copyright

 -- Robert Lemmen <robertle@semistable.com>  Sun, 22 Feb 2004 15:08:55 +0100

xtermcontrol (2.3-1) unstable; urgency=low

  * Initial Release. (closes: #226797)

 -- Robert Lemmen <robertle@semistable.com>  Mon, 12 Jan 2004 17:05:28 +0100
